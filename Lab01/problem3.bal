// Lab01 Problem 3
import ballerina/io;

//create a student record

type Student record {
    string fullName;
    int studentNumber;
    string courseCode;
    string designation;
};

//create a student object from the record
Student student1 = {
    fullName: "John Doe",
    studentNumber: 12345,
    courseCode: "CSCI",
    designation: "BSc"
};

public function main() {

    //print student details
    io:println("STUDENT DETAILS\n");
    io:println("Full Name: " + student1.fullName,
        "\n" + "Student Number: " + student1.studentNumber.toJsonString(),
        "\n" + "Course Code: " + student1.courseCode,
        "\n" + "Designation: " + student1.designation);
}
